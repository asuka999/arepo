#!/usr/bin/env node

import {spawn} from 'child_process'
import fs from 'fs'
import path from 'path'
import program from 'commander'
import merge from 'lodash.merge'
import {version} from '../package.json'
import packageJson from '../constants/package.json'
import {promisify, safeJson} from './helper'

const exec = promisify((cmd, cb) => {
  const [finalCmd, ...args] = cmd.split(' ')
  const proc = spawn(finalCmd, args, {stdio: 'pipe'})
  proc.stdout.pipe(process.stdout)
  proc.stderr.pipe(process.stderr)
  process.stdin.pipe(proc.stdin)
  proc.on('close', cb)
  return proc
})

const noop = (err) => console.log('ererer', err)

program
  .version(version)

program.parse(process.argv)

;(async () => {
  process.stdout.write('create a es repo\n')
  let pkgInit = await (promisify(fs.readFile)('./package.json').catch(noop))
  if (!pkgInit) {
    await exec('npm init')
    pkgInit = await (promisify(fs.readFile)('./package.json').catch(noop))
  }
  process.stdout.write('write packageJson\n')
  await (promisify(fs.writeFile)('./package.json', safeJson.stringify(
    merge({}, safeJson.parse(pkgInit), packageJson)))
    .catch(err => process.stderr.write(null, err)))
  process.stdout.write('init: cp files\n')
  await exec(`cp -R ${path.resolve(__dirname, '../repo')}/ ./`)
  await exec('mkdir src')
  process.stdout.write('install:\n')
  await exec('npm install')
  process.exit()
})()
