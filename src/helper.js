export function promisify (func) {
  return (...args) => new Promise((resolve, reject) => func(...args,
    (err, ...data) => err ? reject(err) : resolve(data.length < 2 ? data[0] : data))
  )
}

export const safeJson = {
  stringify (...args) {
    try {
      return JSON.stringify(...args)
    } catch (e) {
      return ''
    }
  },
  parse (...args) {
    try {
      return JSON.parse(...args)
    } catch (e) {
      return null
    }
  }
}
